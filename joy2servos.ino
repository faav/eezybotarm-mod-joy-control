#include <Servo.h>

Servo ServoGarra;
Servo Servo2;
Servo Servo3;
Servo ServoBase;

int servoGarraPosicion = 90;
int servo2Posicion = 90;
int servo3Posicion = 90;
int servoBasePosicion = 90;

int joy1_xVel = 0;
int joy1_yVel = 0;

int joy2_xVel = 0;
int joy2_yVel = 0;

int joy1_xPin = A0;
int joy1_yPin = A1;

int joy2_xPin = A2;
int joy2_yPin = A3;

int servoGarraPin = 12;
int servo2Pin = 11;

int servo3Pin = 10;
int servoBasePin = 9;

void setup()
{

  pinMode(servoGarraPin,OUTPUT);
  ServoGarra.attach(servoGarraPin);

  pinMode(servo2Pin,OUTPUT);
  Servo2.attach(servo2Pin);

  pinMode(servo3Pin,OUTPUT);
  Servo3.attach(servo3Pin);

  pinMode(servoBasePin,OUTPUT);
  ServoBase.attach(servoBasePin);

  //Inizialize Serial
  Serial.begin(9600);
}

void loop()
{
  joy1_xVel = (analogRead(joy1_xPin) - 512) / 50;
  servoGarraPosicion = constrain((servoGarraPosicion + joy1_xVel), 1, 180);
  joy1_yVel = (analogRead(joy1_yPin) - 512) / -50;
  servo2Posicion = constrain((servo2Posicion + joy1_yVel), 1, 180);



  joy2_xVel = (analogRead(joy2_xPin) - 512) / 50;
  servo3Posicion = constrain((servo3Posicion + joy2_xVel), 1, 180);
  joy2_yVel = (analogRead(joy2_yPin) - 512) / 50;
  servoBasePosicion = constrain((servoBasePosicion + joy2_yVel), 1, 180);

  //Serial.println(servoGarraPosicion);
  ServoGarra.write(servoGarraPosicion);

  //Serial.println(servo2Posicion);
  Servo2.write(servo2Posicion);

  //Serial.println(servo3Posicion);
  Servo3.write(servo3Posicion);

  //Serial.println(servo3Posicion);
  ServoBase.write(servoBasePosicion);
  

  outputJoystick();
  delay(50);
}


/**
* Display joystick values
*/
void outputJoystick(){
  /*
    Serial.print ("X: "); 
    Serial.print(analogRead(joy2_xVel));
    Serial.print ("/ Y: "); 
    Serial.print(analogRead(joy2_yVel));
    Serial.println (" --- ");
    */
}

